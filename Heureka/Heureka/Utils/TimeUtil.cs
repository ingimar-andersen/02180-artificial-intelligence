﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heureka.Utils
{
    public class TimeUtil
    {
        public static long TicksToNanoseconds(long ticks)
        {
            return (long) Math.Round(1000000000.0 * (double)ticks / Stopwatch.Frequency);
        }
    }
}
