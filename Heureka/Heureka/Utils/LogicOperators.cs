﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heureka.Utils
{
    class LogicOperators
    {
        public enum Operator
        {
            OR = '∨',
            AND = '∧',
            NOT = '¬',
            IMPLIES = '⇒',
            IFF = '⇔',
        }
        public const char OR = '∨';
        public const char AND = '∧';
        public const char NOT = '¬';
        public const char IMPLIES = '⇒';
        public const char IFF = '⇔';

        public static List<char> AllOperators = Enum.GetValues(typeof(Operator)).Cast<Operator>().Select(x => x.ToString().First()).ToList();

    }
}
