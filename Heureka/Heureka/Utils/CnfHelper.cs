﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Heureka.Utils.LogicOperators;

namespace Heureka.Utils
{
    class CnfHelper
    {

        public static string ToCnf(string sentence)
        {
            sentence = NormalizeOperatorSigns(sentence);
            var clauses = new List<string>();
            //Elimnate IFF
            var iffSplit = sentence.Split(LogicOperators.IFF).ToList();
            if(iffSplit.Count() == 2)
            {
                clauses.Add(iffSplit[0] + LogicOperators.IMPLIES + iffSplit[1]);
                clauses.Add(iffSplit[1] + LogicOperators.IMPLIES + iffSplit[0]);

                iffSplit = clauses;
            }
            else if(iffSplit.Count() > 2)
            {
                throw new Exception("Only one IFF operator supported.");
            }
            else
            {
                clauses = iffSplit;
            }
            String output = "";
            //Eliminate IMPLIES
            for (int i = 0; i < clauses.Count(); i++)
            {
                var impliesSplit = clauses[i].Split(LogicOperators.IMPLIES);

                if (impliesSplit.Count() <= 2 && impliesSplit.Count() > 0)
                {
                    //DeMorgan
                    var splitChars = new char[] { LogicOperators.AND, LogicOperators.OR };
                    var literals = impliesSplit[0].Split(splitChars);
                    var operators = impliesSplit[0].Where(x => splitChars.Contains(x)).ToList();
                    for (int j = 0; j < operators.Count(); j++)
                    {
                        if (operators[j] == LogicOperators.AND)
                        {
                            operators[j] = LogicOperators.OR;
                        }
                        else if (operators[j] == LogicOperators.OR)
                        {
                            operators[j] = LogicOperators.AND;
                        }
                    }
                    for (int j = 0; j < literals.Count(); j++)
                    {
                        literals[j] = LogicOperators.NOT + literals[j];
                        literals[j] = literals[j].Replace(LogicOperators.NOT.ToString() + LogicOperators.NOT.ToString(), ""); //Remove double negation
                    }
                    output += "(";
                    for (int j = 0; j < literals.Count(); j++)
                    {
                        output += literals[j];
                        output += j < operators.Count() ? " " + operators[j] + " " : "";
                    }
                    output += ")";
                    if(impliesSplit.Count() > 1)
                    {
                        output += String.Format(" {0} ({1})", LogicOperators.AND, impliesSplit[1]);
                    }
                    if (i < clauses.Count - 1)
                    {
                        output += String.Format(" {0} ", LogicOperators.AND);
                    }
                }
                else if(impliesSplit.Count() > 2)
                {
                    throw new Exception("Only one IMPLICIT operator supported.");
                }
                   
            }
            return output;
        }

        public static string NormalizeOperatorSigns(string input)
        {
            var inputArray = input.ToCharArray();
            if (inputArray.Contains('(') || inputArray.Contains(')')) {
                throw new Exception("Parenthesis not supported");
            }
            //With spaces
            input = input.Replace(" V ", LogicOperators.OR.ToString());
            input = input.Replace(" v ", LogicOperators.OR.ToString());
            input = input.Replace(" OR ", LogicOperators.OR.ToString());
            input = input.Replace(" or ", LogicOperators.OR.ToString());
            input = input.Replace(" AND ", LogicOperators.AND.ToString());
            input = input.Replace(" and ", LogicOperators.AND.ToString());
            input = input.Replace(" NOT ", LogicOperators.NOT.ToString());
            input = input.Replace(" not ", LogicOperators.NOT.ToString());

            //Remove spaces
            input = input.Replace(" ", "");

            //Normal operator signs
            input = input.Replace("<=>", LogicOperators.IFF.ToString());
            input = input.Replace("<->", LogicOperators.IFF.ToString());
            input = input.Replace('≡', LogicOperators.IFF);
            input = input.Replace('↔', LogicOperators.IFF);

            input = input.Replace("->", LogicOperators.IMPLIES.ToString());
            input = input.Replace("=>", LogicOperators.IMPLIES.ToString());
            input = input.Replace('→', LogicOperators.IMPLIES);
            input = input.Replace('⊃', LogicOperators.IMPLIES);

            input = input.Replace('+', LogicOperators.OR);
            input = input.Replace('∥', LogicOperators.OR);
            input = input.Replace('|', LogicOperators.OR);

            input = input.Replace('&', LogicOperators.AND);
            input = input.Replace('*', LogicOperators.AND);
            input = input.Replace('·', LogicOperators.AND);
            input = input.Replace('^', LogicOperators.AND);

            input = input.Replace('!', LogicOperators.NOT);
            input = input.Replace('-', LogicOperators.NOT);
            input = input.Replace('˜', LogicOperators.NOT);

            return input;
        }
    }
}
