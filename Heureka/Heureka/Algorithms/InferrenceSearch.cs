﻿using Heureka.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heureka.Algorithms
{
    class InferrenceSearch
    {

        public static bool ResolutionByRefutation(Clause startClause, List<Clause> kb, bool useAncestors)
        {
            //Goal is the empty clause
            
            Priority_Queue.SimplePriorityQueue<Clause> frontier = new Priority_Queue.SimplePriorityQueue<Clause>();

            startClause.PathCost = 0;
            startClause.SideClauses = kb;
            frontier.Enqueue(startClause, startClause.PathCost);
            int iterationCount = 0;
            while (frontier.Any())
            {
                iterationCount++;
                var current = frontier.First();

                //Goal check
                if (current.IsEmptyClause())
                {
                    Console.WriteLine(String.Format("Iterations: {0}", iterationCount));
                    return true; //Contradiction
                }
                frontier.Dequeue();
                for(int i = 0; i < current.SideClauses.Count(); i++)
                {
                    var sideClause = current.SideClauses[i];
                    var resolvedLiterals = current.Resolve(sideClause);

                    //Only add clauses to the frontier that can be resolved.
                    if(resolvedLiterals != null)
                    {
                        var heuristicCost = current.PathCost + 1 + resolvedLiterals.Count();
                        var newSideClauses = current.SideClauses.Where((x, index) => index != i).ToList(); //All previous sideclauses except itself.
                        if (useAncestors)
                        {
                            newSideClauses.Add(current);
                        }

                        frontier.Enqueue(new Clause(resolvedLiterals, newSideClauses, heuristicCost), heuristicCost);
                    }
                }
            }
            Console.WriteLine(String.Format("Iterations: {0}", iterationCount));
            Console.WriteLine("Could not disprove negated clause.");
            return false;
        }

        private static bool goalCheck(Clause startClause, Clause current)
        {
            var result = false;
            Console.WriteLine(String.Format("\t\tGoal Check: {0} and {1}", startClause.ToString(), current.ToString()));
            if (startClause.Literals.Count() == current.Literals.Count())
            {
                var oppositeLiterals = startClause.Literals.Where(x => current.Literals.Any(y => y.Name == x.Name && y.IsNegated != x.IsNegated)).Count();
                result = oppositeLiterals == startClause.Literals.Count();
            }
            Console.WriteLine(String.Format("\t\tGoal Check result: {0}", result));
            return result;
        }

    }
}
