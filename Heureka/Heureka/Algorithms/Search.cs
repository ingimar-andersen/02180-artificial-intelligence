﻿using Heureka.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heureka.Algorithms
{
    public class GraphSearch
    {

        private Graph Graph;

        private Node Start;
        private Node Goal;

        public GraphSearch(Graph graph)
        {
            this.Graph = graph;
        }


        public List<Edge> AStar(int startID, int goalID, HeuristicType hType, bool debug = false)
        {
            ResetNodes();
            Priority_Queue.FastPriorityQueue<Node> frontier = new Priority_Queue.FastPriorityQueue<Node>(Graph.Nodes.Count);
            Start = Graph.Nodes[startID];
            Goal = Graph.Nodes[goalID];
            Start.DistanceFromSource = 0;
            //float hCost = Start.Position.DistanceTo(Goal.Position, hType);
            Start.SourceToGoal = float.MaxValue;

            frontier.Enqueue(Start, Start.SourceToGoal);
            int iterationCount = 0;
            while (frontier.Any())
            {
                iterationCount++;
                var current = frontier.First();
                if (current.Position.IsEqual(Goal.Position))
                {
                    if (debug)
                    {
                        Console.WriteLine(String.Format("Iterations: {0}", iterationCount));
                    }

                    return ReconstructPath(current);
                }
                frontier.Dequeue();
                Graph.Nodes[current.ID].IsVisited = true;
                foreach (var edge in Graph.IncidentListByNodeID[current.ID])
                {
                    edge.IsVisited = true; //For graph drawing

                    Node neighbour = null;
                    if (Graph.IsDirected)
                    {
                        neighbour = Graph.Nodes[edge.ToID];
                    }
                    else
                    {
                        //Undiredcted
                        neighbour = Graph.Nodes[edge.ToID == current.ID ? edge.FromID : edge.ToID];
                    }

                    if (neighbour.IsVisited)
                    {
                        continue;
                    }

                    var newDistanceFromSource = current.DistanceFromSource + edge.Distance ?? current.Position.DistanceTo(neighbour.Position, HeuristicType.Euclidean); //For datasets without edge distance the distance of the direct line is calculated.

                    //Discover a new node
                    float? newSourceToGoal = null;
                    if (!frontier.Contains(neighbour))
                    {
                        newSourceToGoal = newDistanceFromSource + neighbour.Position.DistanceTo(Goal.Position, hType);
                        frontier.Enqueue(neighbour, neighbour.SourceToGoal);
                        //neighbour.SourceToGoal = (float)newSourceToGoal;
                    }
                    if (newDistanceFromSource >= neighbour.DistanceFromSource)
                    {
                        
                        continue; //Not a better path
                    }
                    //Is the best path until now
                    neighbour.CameFrom = current.ID;
                    neighbour.EdgeID = edge.ID;
                    neighbour.DistanceFromSource = newDistanceFromSource;
                    neighbour.SourceToGoal = newSourceToGoal ?? newDistanceFromSource + neighbour.Position.DistanceTo(Goal.Position, hType);
                    frontier.UpdatePriority(neighbour, neighbour.SourceToGoal);
                }
            }
            if (debug)
            {
                Console.WriteLine(String.Format("Iterations: {0}", iterationCount));
                Console.WriteLine("Goal could not be found");
            }

            return new List<Edge>();
        }

        private static Drawing.GraphDrawer gd;
        public List<Edge> RecursiveBestFirst(int startID, int goalID, HeuristicType hType, Drawing.BoundingBox bb = null, bool debug = false)
        {
            ResetNodes();
            if (debug && bb != null)
            {
                gd = new Drawing.GraphDrawer(Graph, bb, null);
            }
            Start = Graph.Nodes[startID];
            Goal = Graph.Nodes[goalID];
            Start.DistanceFromSource = 0;
            float hCost = Start.Position.DistanceTo(Goal.Position, hType);
            Start.SourceToGoal = hCost;

            var root = new TreeNode<Node>(Start, null);
            var result = RBFS(root, float.MaxValue, hType, debug);

            return result.Result;
        }

        private static int recursionCount = 0;
        private bool done = false;
        private int MAX_RECURSION = int.MaxValue;

        private RbfsResult RBFS(TreeNode<Node> current, float fLimit, HeuristicType hType, bool debug)
        {
            if (debug && recursionCount % 10000000 == 0)
            {
                Console.WriteLine("Recursion: {0} million", recursionCount / 1000000);
                if (gd != null)
                {
                    gd.DrawGraph("graphB" + recursionCount, 3200, 3200);
                }
            }
            if (recursionCount > MAX_RECURSION)
            {
                done = true;
                return new RbfsResult(float.MaxValue, RbfsResultType.Failure);
            }
            recursionCount++;
            current.Item.IsVisited = true;

            //Goal check
            if (current.Item.Position.IsEqual(Goal.Position))
            {
                var result = ReconstructPathRBFS(current);
                return new RbfsResult(current.Item.SourceToGoal, RbfsResultType.Success, result);
            }

            //Neighbours
            var edges = Graph.IncidentListByNodeID[current.Item.ID];
            foreach (var edge in edges)
            {
                edge.IsVisited = true;
                Node successorNode = null;
                if (Graph.IsDirected)
                {
                    successorNode = Graph.Nodes[edge.ToID];
                }
                else
                {
                    successorNode = Graph.Nodes[edge.ToID == current.Item.ID ? edge.FromID : edge.ToID]; //Undiredcted
                }

                //Do not check if successor is parent
                if (CheckIfAncestor(current, successorNode))
                {
                    continue;
                }

                var newDistanceFromSource = current.Item.DistanceFromSource + edge.Distance ?? current.Item.Position.DistanceTo(successorNode.Position, hType); //For datasets without edge distance the distance of the direct line is calculated.
                if (newDistanceFromSource > successorNode.DistanceFromSource)
                {
                    continue; //Not a better path
                }
                successorNode.DistanceFromSource = newDistanceFromSource;
                successorNode.SourceToGoal = newDistanceFromSource + successorNode.Position.DistanceTo(Goal.Position, hType);
                successorNode.SourceToGoal = Math.Max(successorNode.SourceToGoal, current.Item.SourceToGoal);
                current.AddChild(successorNode, successorNode.SourceToGoal);
            }
            if (!current.Children.Any())
            {
                return new RbfsResult(float.MaxValue, RbfsResultType.Failure);
            }

            while (true)
            {
                var twoBest = current.Children.Take(2);
                var best = twoBest.FirstOrDefault();
                if (!current.Children.Any())
                {
                    return new RbfsResult(float.MaxValue, RbfsResultType.Failure);
                }
                else if (best.Item.SourceToGoal > fLimit)
                {
                    current.Children.Clear(); //Remove children on failure
                    return new RbfsResult(best.Item.SourceToGoal, RbfsResultType.Failure);
                }
                var alternative = twoBest.ElementAtOrDefault(1); //Only two elements
                var fAlternative = alternative == null ? float.MaxValue : alternative.Item.SourceToGoal;

                if (debug)
                {
                    var bestEdge = Graph.IncidentListByNodeID[current.Item.ID].Single(x => x.ToID == best.Item.ID);
                    Console.WriteLine("Best. From \t{0} \tTo \t{1} \t{2} \t Cost: {3} ", current.Item.Position.ToString(), best.Item.Position.ToString(), bestEdge.Name, best.Item.SourceToGoal);
                }
                var result = RBFS(best, Math.Min(fLimit, fAlternative), hType, debug);
                if (result.ResultType == RbfsResultType.Failure)
                {
                    if (done)
                    {
                        return result;
                    }
                    best.Item.SourceToGoal = result.FCost;
                    current.Children.UpdatePriority(best, result.FCost);
                }
                else
                {
                    return result;
                }
            }
        }

        private bool CheckIfAncestor(TreeNode<Node> current, Node successorNode)
        {
            var parent = current.Parent;
            while (parent != null)
            {
                if (successorNode.ID == parent.Item.ID)
                {
                    return true;
                }
                parent = parent.Parent;
            }
            return false;
        }

        private List<Edge> ReconstructPathRBFS(TreeNode<Node> node)
        {
            var result = new List<Edge>();
            var currentNode = node;
            while (currentNode.Parent != null)
            {
                Edge edge = null;
                if (Graph.IsDirected)
                {
                    edge = Graph.IncidentListByNodeID[currentNode.Parent.Item.ID].Single(x => x.ToID == currentNode.Item.ID);
                }
                else
                {

                    edge = Graph.IncidentListByNodeID[currentNode.Parent.Item.ID].FirstOrDefault(x => x.ToID == currentNode.Item.ID);
                    if (edge == null)
                    {
                        edge = Graph.IncidentListByNodeID[currentNode.Item.ID].FirstOrDefault(x => x.ToID == currentNode.Parent.Item.ID);
                    }
                }

                edge.IsPath = true;
                result.Add(edge);
                currentNode = currentNode.Parent;
            }
            result.Reverse();
            return result;
        }

        private List<Edge> ReconstructPath(Node node)
        {
            var result = new List<Edge>();
            var current = node;
            while (current.EdgeID != null)
            {
                var edge = Graph.Edges[(int)current.EdgeID];
                edge.IsPath = true;
                result.Add(edge);

                current = Graph.Nodes[(int)current.CameFrom];
            }
            result.Reverse();
            return result;
        }

        private void ResetNodes()
        {
            foreach (var node in Graph.Nodes.Values)
            {
                node.EdgeID = null;
                node.CameFrom = null;
                node.DistanceFromSource = float.MaxValue;
                node.SourceToGoal = float.MaxValue;
                node.IsVisited = false;
            }
        }
    }

    internal class RbfsResult
    {
        public float FCost;
        public RbfsResultType ResultType;
        public List<Edge> Result;

        internal RbfsResult(float fCost, RbfsResultType resultType, List<Edge> result = null)
        {
            this.FCost = fCost;
            this.ResultType = resultType;
            this.Result = result;
        }
    }

    internal enum RbfsResultType
    {
        Failure,
        Success,
    }
}
