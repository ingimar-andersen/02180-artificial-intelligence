﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Heureka.Model;
using Heureka.Drawing;
using Heureka.Algorithms;
using System.Diagnostics;
using Heureka.Utils;

namespace Heureka
{
    class Program
    {
        internal static Graph Graph = new Graph();

        internal static BoundingBox BB = new BoundingBox();

        static void Main(string[] args)
        {
            ShowMenu();
        }

        private static void ShowMenu(bool printMenuList = true)
        {
            if (printMenuList)
            {
                Console.WriteLine("You have the following options" + Environment.NewLine);
                Console.WriteLine("0 - Exit the program");
                Console.WriteLine("1 - Graph search");
                Console.WriteLine("2 - Inference engine" + Environment.NewLine);
                Console.WriteLine("Enter corresponding number:");
            }

            var input = Console.ReadLine().ToCharArray().First();
            if (input == '0')
            {
                return;
            }
            else if (input == '1')
            {
                EnterGraphSearch();
            }
            else if (input == '2')
            {
                EnterInferenceEngine();
            }
            else
            {
                Console.WriteLine("Incorrect input. Try again:");
                ShowMenu(false);
            }
        }

        private static void EnterInferenceEngine()
        {
            //CnfHelper.ToCnf("B11 <=> P11 or P21");

            var kb = new List<Clause>();
            //kb.Add(new Clause(new string[] { "!b", "a" }));
            //kb.Add(new Clause(new string[] { "a", "b", "e" }));
            //kb.Add(new Clause(new string[] { "!b", "a"}));
            //kb.Add(new Clause(new string[] { "a", "b" }));

            //kb.Add(new Clause(new string[] { "a", "!b", "!c" }));
            //kb.Add(new Clause(new string[] { "2b", "!2b"}));
            //kb.Add(new Clause(new string[] { "b", "!c", "!d" }));
            //kb.Add(new Clause(new string[] { "c" }));
            //kb.Add(new Clause(new string[] { "d" }));

            //Test for use of ancestors. Will find solution with ancestors but not without.
            kb.Add(new Clause(new string[] { "a", "!b"}));
            kb.Add(new Clause(new string[] { "b", "!c" }));
            kb.Add(new Clause(new string[] { "c", "b"}));

            Console.WriteLine("Knowledge base");
            foreach(var clause in kb)
            {
                Console.WriteLine(String.Format("\t ({0})", clause.ToString()));
            }
            Console.WriteLine();

            var result = InferrenceSearch.ResolutionByRefutation(new Clause(new string[] { "!a"}), kb, true);
            Console.WriteLine(result);

            ShowMenu();

        }

        private static void EnterGraphSearch()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            ParseDataUndirected();
            //ParseDataDirected();

            sw.Stop();
            var parseTime = TimeUtil.TicksToNanoseconds(sw.ElapsedTicks);
            sw.Restart();

            var search = new GraphSearch(Graph);
            var hType = HeuristicType.Manhattan;
            //var path = search.RecursiveBestFirst(0, 17000, hType); //Gott SanFran test
            //var path = search.AStar(70000, 165000, hType); //US Vestur -> Austur

            //"Try for instance to find the car route from the corner of SktPedersStraede & Larsbjoernsstraede to the corner of Studiestraede & Larsbjoernsstraede."
            //(35, 80) to (45, 70) that are nodes 3 -> 6
            //var path = search.AStar(3, 6, hType);
            //var path = search.AStar(0, 13, hType);

            //Manhattan
            List<Edge> path = null;
            long searchTime;
            var timingList = new List<long>();
            //for(int i = 0; i < 10000000; i++)
            //{
                path = search.AStar(0, 1000, hType, false);

                //var path = search.RecursiveBestFirst(0, 17000, hType, BB);
                //var path = search.AStar(0, 17000, hType);
                sw.Stop();
                searchTime = TimeUtil.TicksToNanoseconds(sw.ElapsedTicks);
                timingList.Add(searchTime);
                sw.Restart();
            //}
            searchTime = (long) Math.Round(timingList.Average());

            var gd = new GraphDrawer(Graph, BB, path);
            gd.DrawGraph("graph");

            sw.Stop();
            var drawGraphTime = TimeUtil.TicksToNanoseconds(sw.ElapsedTicks);

            Console.WriteLine(String.Format("Parse time: {0} Search time: {1} Draw time: {2} Total time: {3}  [ns]" + Environment.NewLine, parseTime, searchTime, drawGraphTime, parseTime + searchTime + drawGraphTime));

            PrintPath(path);
            ShowMenu();
        }

        private static void PrintPath(List<Edge> path)
        {
            double totalLength = 0;
            foreach (var edge in path)
            {
                Console.WriteLine(String.Format("ID: {0} \tName: {1}", edge.ID, edge.Name));
                totalLength += (double) edge.Distance;
            }
            Console.WriteLine();
            Console.WriteLine(String.Format("Total path length: {0}, Number of edges: {1}", totalLength.ToString("0.00"), path.Count()));
        }

        //For data from course
        private static void ParseDataDirected()
        {
            try
            {
                Graph.IsDirected = true;
                string current_path = System.IO.Path.GetDirectoryName(Environment.CurrentDirectory);
                string[] InputLines = System.IO.File.ReadAllLines(current_path + "/Debug/Data/Manhattan.txt");
                foreach (var line in InputLines)
                {
                    var param = line.Split(' ');
                    var from = new Model.Point(int.Parse(param[0]), int.Parse(param[1]));
                    var fromNode = CreateNode(from);
                    var streetName = param[2];

                    var to = new Model.Point(int.Parse(param[3]), int.Parse(param[4]));
                    var toNode = CreateNode(to);

                    Edge edge = CreateEdge(fromNode.ID, toNode.ID, streetName);

                    List<Edge> value;
                    if (Graph.IncidentListByNodeID.TryGetValue(fromNode.ID, out value))
                    {
                        value.Add(edge);
                    }
                    else
                    {
                        Graph.IncidentListByNodeID.Add(fromNode.ID, new List<Edge>() { edge });
                    }

                    BB.UpdateFromNode(fromNode);
                    BB.UpdateFromNode(toNode);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("Input parsing failed with the following message: {0}", e.Message));
            }
        }

        //For data from internet
        private static void ParseDataUndirected()
        {
            try
            {
                Graph.IsDirected = false;
                string current_path = System.IO.Path.GetDirectoryName(Environment.CurrentDirectory);
                string[] InputLines = System.IO.File.ReadAllLines(current_path + "/Debug/Data/SanFranRoadMapEdges.txt");
               
                foreach (var line in InputLines)
                {
                    var param = line.Split(' ');

                    var edge = new Edge(int.Parse(param[0]), int.Parse(param[1]), int.Parse(param[2]), float.Parse(param[3], CultureInfo.InvariantCulture));
                    Graph.Edges.Add(edge.ID, edge);

                    AddToIncidentList(edge.FromID, edge);
                    AddToIncidentList(edge.ToID, edge);
                }

                InputLines = System.IO.File.ReadAllLines(current_path + "/Debug/Data/SanFranRoadMapNodes.txt");

                foreach (var line in InputLines)
                {
                    var param = line.Split(' ');

                    var node = new Node(int.Parse(param[0]), (float)double.Parse(param[1], CultureInfo.InvariantCulture), (float)double.Parse(param[2], CultureInfo.InvariantCulture));
                    Graph.Nodes.Add(node.ID, node);
                    BB.UpdateFromNode(node);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("Input parsing failed with the following message: {0}", e.Message));
            }
        }

        private static int edgeID = 0;
        private static Edge CreateEdge(int fromId, int toId, string streetName)
        {
            Edge edge = new Edge(edgeID, fromId, toId, null, streetName);
            if (!Graph.Edges.Values.Any(x => x.IsEqual(edge)))
            {
                edge.ID = edgeID;
                Graph.Edges.Add(edge.ID, edge);
                edgeID++;

                return edge;
            }
            else
            {
                return Graph.Edges.Values.Single(x => x.IsEqual(edge));
            }
        }

        private static int nodeID = 0;
        private static Node CreateNode(Model.Point p)
        {
            Node node;
            if (!Graph.Nodes.Values.Any(x => x.Position.IsEqual(p)))
            {
                node = new Node(nodeID, p);
                Graph.Nodes.Add(node.ID, node);
                nodeID++;
            }
            else
            {
                node = Graph.Nodes.Values.Single(x => x.Position.IsEqual(p));
            }
            return node;
        }

        private static void AddToIncidentList(int nodeID, Edge edge)
        {
            List<Edge> value;
            if (Graph.IncidentListByNodeID.TryGetValue(nodeID, out value))
            {
                value.Add(edge);
            }
            else
            {
                Graph.IncidentListByNodeID.Add(nodeID, new List<Edge>() { edge });
            }
        }
    }
}
