﻿using Priority_Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heureka.Model
{
    public class Node : FastPriorityQueueNode, IComparable
    {
        public int ID;
        public Point Position;
        public float DistanceFromSource { get; set; } = float.MaxValue;   // g(n)
        public float SourceToGoal { get; set; } = float.MaxValue;         // f(n) = g(n) + h(n). Heuristic path distance to goal
        public int? CameFrom = null;
        public int? EdgeID = null;
        public bool IsVisited = false;

        public Node(int id, float x, float y)
        {
            this.ID = id;
            this.Position = new Point(x, y);
        }

        public Node(int id, Point p)
        {
            this.ID = id;
            this.Position = p;
        }

        public int CompareTo(object obj)
        {
            if (obj.GetType() != typeof(Node))
            {
                throw new NotImplementedException();
            }

            //Same Element
            if (this.ID == (obj as Node).ID)
            {
                return 0;
            }
            //Ascending order
            if (this.SourceToGoal < (obj as Node).SourceToGoal)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }

        public bool IsEqual(Node node)
        {
            return this.ID == node.ID;
        }
    }
}
