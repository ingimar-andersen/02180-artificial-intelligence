﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heureka.Model
{
    public class Edge : IComparable
    {
        public int ID;
        public int FromID;
        public int ToID;
        public float? Distance;
        public bool IsPath;
        public bool IsVisited;
        public string Name;

        public Edge(int id, int fromID, int toId, float? distance, string name = "")
        {
            this.ID = id;
            this.FromID = fromID;
            this.ToID = toId;
            this.Distance = distance;
            this.Name = name;
            this.IsPath = false;
            this.IsVisited = false;
        }

        public bool IsEqual(Edge edge)
        {
            return this.ID == edge.ID;
        }

        public int CompareTo(object obj)
        {
            if (obj.GetType() != typeof(Edge))
            {
                throw new NotImplementedException();
            }

            //Same Element
            if (this.ID == (obj as Edge).ID)
            {
                return 0;
            }
            //Ascending order
            if (this.ID < (obj as Edge).ID)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }
    }
}
