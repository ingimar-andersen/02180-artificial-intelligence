﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heureka.Model
{
    public class Point
    {
        public float X;
        public float Y;

        public Point(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }

        public float DistanceTo(Point p, HeuristicType heuristicType)
        {
            if (heuristicType == HeuristicType.Euclidean) {
                return (float)Math.Sqrt(Math.Pow(this.X - p.X, 2) + Math.Pow(this.Y - p.Y, 2));
            }
            else if (heuristicType == HeuristicType.Manhattan)
            {
                return Math.Abs(this.X - p.X) + Math.Abs(this.Y - p.Y);
            }
            else
            {
                throw new NotImplementedException(String.Format("Distance for heuristic type '{0}' not implemented.", heuristicType.ToString()));
            }
        }

        public bool IsEqual(Point point)
        {
            return this.X == point.X && this.Y == point.Y;
        }

        public override string ToString()
        {
            return String.Format("({0},{1})", X, Y);
        }
    }
}
