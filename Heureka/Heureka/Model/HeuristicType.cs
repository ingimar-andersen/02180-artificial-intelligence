﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heureka.Model
{
    public enum HeuristicType
    {
        Euclidean = 1,
        Manhattan = 2,
    }
}
