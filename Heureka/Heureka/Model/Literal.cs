﻿using Heureka.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heureka.Model
{
    public class Literal
    {
        private String _Name;
        private bool _IsNegated;

        public string Name
        {
            get
            {
                return _Name;
            }
            private set
            {
                _Name = value;
            }
        }

        public bool IsNegated
        {
            get
            {
                return _IsNegated;
            }

            private set
            {
                _IsNegated = value;
            }
        }

        public Literal(String name, bool negation)
        {
            this.Name = name;
            this.IsNegated = negation;
        }
        public Literal(Literal literal)
        {
            this.Name = literal.Name;
            this.IsNegated = literal.IsNegated;
        }

        public void Negate()
        {
            this.IsNegated = !this.IsNegated;
        }

        public static Literal Copy(Literal literal)
        {
            return new Literal(literal.Name, literal.IsNegated);
        }

        public override string ToString()
        {
            return IsNegated ? LogicOperators.NOT + Name : Name;
        }

    }
}
