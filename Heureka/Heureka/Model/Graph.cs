﻿using Heureka.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heureka.Model
{
    public class Graph
    {
        public Dictionary<int, Edge> Edges = new Dictionary<int, Edge>();
        public Dictionary<int, Node> Nodes = new Dictionary<int, Node>();
        public Dictionary<int, List<Edge>> IncidentListByNodeID = new Dictionary<int, List<Edge>>();

        public bool IsDirected;

        public Graph(Dictionary<int, Edge> edges, Dictionary<int, Node> nodes, Dictionary<int, List<Edge>> incidentListByNodeID, bool directed)
        {
            this.Edges = edges;
            this.Nodes = nodes;
            this.IncidentListByNodeID = incidentListByNodeID;
            this.IsDirected = directed;
        }

        public Graph()
        {
            this.Edges = new Dictionary<int, Edge>();
            this.Nodes = new Dictionary<int, Node>();
            this.IncidentListByNodeID = new Dictionary<int, List<Edge>>();
        }
    }
}
