﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heureka.Model
{
    public class TreeNode<T>
    {
        public TreeNode<T> Parent;
        public Priority_Queue.SimplePriorityQueue<TreeNode<T>> Children = new Priority_Queue.SimplePriorityQueue<TreeNode<T>>();

        public T Item { get; set; }

        public TreeNode(T item, TreeNode<T> parent)
        {
            Item = item;
            Parent = parent;
        }

        public TreeNode<T> AddChild(T item, float priority)
        {
            TreeNode<T> nodeItem = new TreeNode<T>(item, this);
            Children.Enqueue(nodeItem, priority);
            return nodeItem;
        }
    }
}
