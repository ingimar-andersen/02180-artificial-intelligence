﻿using Heureka.Utils;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Heureka.Model
{
    public class Clause
    {
        public List<Literal> Literals;
        public float PathCost;
        public List<Clause> SideClauses;

        public Clause()
        {
            this.PathCost = float.MaxValue;
            Literals = new List<Literal>();
        }

        public Clause(List<Literal> literals)
        {
            this.PathCost = float.MaxValue;
            Literals = literals;
        }

        public Clause(string[] literals)
        {
            this.PathCost = float.MaxValue;
            var literalsList = new List<Literal>();
            foreach(var lit in literals)
            {
                var normLit = CnfHelper.NormalizeOperatorSigns(lit);
                if(normLit.First() == LogicOperators.NOT)
                {
                    literalsList.Add(new Literal(normLit.Substring(1), true));
                }
                else
                {
                    literalsList.Add(new Literal(normLit, false));
                }
                
            }
            Literals = literalsList;
        }

        public Clause(List<Literal> literals, List<Clause> sideClauses, float pathCost)
        {
            this.Literals = literals;
            this.SideClauses = sideClauses;
            this.PathCost = pathCost;
        }

        internal bool IsEmptyClause()
        {
            return !Literals.Any();
        }

        internal List<Clause> ResolveNeighbours(List<Clause> currentNeighbours)
        {
            List<Clause> neighbours = new List<Clause>();
            for (int i = 0; i < currentNeighbours.Count(); i++)
            {
                var sideClause = currentNeighbours[i];
                var sideClauseNeighbours = currentNeighbours.Where((x, index) => index != i).ToList();
                neighbours.Add(new Clause(this.Resolve(sideClause), sideClauseNeighbours, this.PathCost + 1));
            }
            return neighbours;
        }

        public bool isEmpty()
        {
            return Literals.Any();
        }

        public override string ToString()
        {
            if (!Literals.Any()) {
                return "*Empty Clause*";
            }
            string value = "";
            foreach (var literal in Literals)
            {
                if (literal.IsNegated) value += "¬";
                value += literal.Name;
                value += " V ";
            }
            value = value.Substring(0, value.Length - 3); // Remove last " v "
            return value;
        }

        public List<Literal> Resolve(Clause clause)
        {
            Console.WriteLine(String.Format("Trying to resolve ({0}) with ({1}), Heuristic cost: {2}", this.ToString(), clause.ToString(), this.PathCost));
            var oppositeLiterals = Literals.Where(x => clause.Literals.Any(y => y.Name == x.Name && y.IsNegated != x.IsNegated)).ToList();
            if (oppositeLiterals.Count() == 1)
            {
                //reduce opposite duplicate
                var newLits = new List<Literal>();
                newLits.AddRange(CopyLiterals(Literals.Where(x => x.Name != oppositeLiterals.First().Name).ToList()));
                newLits.AddRange(CopyLiterals(clause.Literals.Where(x => x.Name != oppositeLiterals.First().Name).ToList()));

                //reduce duplicates
                newLits = newLits.DistinctBy(x => x.Name).ToList();
                var resultingClause = new Clause(newLits);
                Console.WriteLine(String.Format("\tResolve result: ({0})", resultingClause.ToString()));
                return newLits;
            }
            Console.WriteLine("\tCould not resolve");
            return null;
        }

        private List<Literal> CopyLiterals(List<Literal> literals)
        {
            var result = new List<Literal>();
            foreach (var lit in literals)
            {
                result.Add(Literal.Copy(lit));
            }
            return result;
        }
    }
}