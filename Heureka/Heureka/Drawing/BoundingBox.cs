﻿namespace Heureka.Drawing
{
    public class BoundingBox
    {
        public float MinX;
        public float MaxX;
        public float MinY;
        public float MaxY;

        public BoundingBox()
        {
            this.MinX = float.MaxValue;
            this.MaxX = float.MinValue;
            this.MinY = float.MaxValue;
            this.MaxY = float.MinValue;
        }

        public void UpdateFromNode(Model.Node node)
        {
            if (node.Position.X > this.MaxX)
            {
                this.MaxX = node.Position.X;
            }
            else if (node.Position.X < this.MinX)
            {
                this.MinX = node.Position.X;
            }
            if (node.Position.Y > this.MaxY)
            {
                this.MaxY = node.Position.Y;
            }
            else if (node.Position.Y < this.MinY)
            {
                this.MinY = node.Position.Y;
            }
        }
    }
}
