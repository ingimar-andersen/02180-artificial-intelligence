﻿using Heureka.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heureka.Drawing
{
    public class GraphDrawer
    {
        private Graph Graph;

        private BoundingBox BB;

        private IEnumerable<Edge> Path;

        private float Height;
        private float Width;

        private static float Scaling = 1;
        private int PixelWidth;
        private int PixelHeight;

        public int TriangleWidth = 16;
        public int TriangleHeight = 16;

        public int PaddingPercentage = 5;
        private int Padding;

        private const int THICKNESS_VISITED = 3;
        private const int THICKNESS_PATH = 16;
        private const int THICKNESS_EDGES = 1;

        private Dictionary<int, Edge> TwoWayEdges; //Only for directed graphs

        public GraphDrawer(Graph graph, BoundingBox boundingBox, IEnumerable<Edge> path = null)
        {
            this.Graph = graph;
            this.BB = boundingBox;
            this.Path = path;
        }

        static bool hasScaled = false;

        public void DrawGraph(string fileName, int maxWidth = 0, int maxHeight = 0)
        {

            if (!hasScaled)
            {
                Padding = (int)Math.Floor(((maxHeight + maxWidth) / 2 * (PaddingPercentage / 100.0)));
                maxWidth -= 2 * Padding;
                maxHeight -= 2 * Padding;

                Scale(maxWidth, maxHeight);
                hasScaled = true;
            }

            using (Bitmap bmp = new Bitmap(PixelWidth + 2 * Padding, PixelHeight + 2 * Padding))
            {
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    //Rotates the graphics and centers around Origo to represent ordinary (x,y)-coordinate system.
                    Matrix m = new Matrix(new RectangleF(BB.MinX, BB.MaxY, (BB.MaxX - BB.MinX), -(BB.MaxY - BB.MinY)),
                                new PointF[] {
                            new PointF(Padding, Padding),
                            new PointF(Width + Padding, Padding),
                            new PointF(Padding, Height + Padding)
                                });
                    g.Transform = m;
                    Rectangle rect = new Rectangle(-Padding, -Padding, bmp.Width, bmp.Height);
                    g.FillRectangle(Brushes.White, rect);
                    g.DrawRectangle(new Pen(Color.White), rect);
                    var pencilUnit = (int)Math.Ceiling(Math.Max(PixelWidth, PixelHeight) / 10000.0);
                    //Draw visited
                    foreach (var edge in Graph.Edges.Values.Where(x => x.IsVisited && !x.IsPath))
                    {
                        var from = Graph.Nodes[edge.FromID];
                        var to = Graph.Nodes[edge.ToID];
                        var pen = CreatePen(Color.Red, pencilUnit * THICKNESS_VISITED);
                        DrawLine(g, from, to, pen);
                    }
                    //Draw path
                    if (Path != null)
                    {
                        foreach (var edge in Path)
                        {
                            var from = Graph.Nodes[edge.FromID];
                            var to = Graph.Nodes[edge.ToID];
                            var pen = CreatePen(Color.Green, pencilUnit * THICKNESS_PATH);
                            DrawLine(g, from, to, pen);
                        }
                    }

                    //Draw map
                    if (Graph.IsDirected)
                    {
                        TwoWayEdges = Graph.Edges.SelectMany(x => Graph.IncidentListByNodeID[x.Value.FromID].Where(y => Graph.IncidentListByNodeID[y.ToID].Any(z => z.ToID == y.FromID))).Distinct().ToDictionary(edge => edge.ID);
                    }

                    using (GraphicsPath capPath = new GraphicsPath())
                    {
                        // A triangle
                        capPath.AddLine(-TriangleWidth / 2, -TriangleHeight, 0, 0);
                        capPath.AddLine(TriangleWidth / 2, -TriangleHeight, 0, 0);
                        capPath.AddLine(-TriangleWidth / 2, -TriangleHeight, TriangleWidth / 2, -TriangleHeight);

                        foreach (var edge in Graph.Edges.Values)
                        {
                            var from = Graph.Nodes[edge.FromID];
                            var to = Graph.Nodes[edge.ToID];
                            var pen = CreatePen(Color.Black, pencilUnit * THICKNESS_EDGES);
                            if (Graph.IsDirected)
                            {
                                if (!TwoWayEdges.ContainsKey(edge.ID))
                                {
                                    pen.CustomEndCap = new System.Drawing.Drawing2D.CustomLineCap(null, capPath);
                                }
                                else
                                {
                                    pen.EndCap = LineCap.Round;
                                }
                            }

                            DrawLine(g, from, to, pen);
                        }
                    }
                }
                bmp.Save(fileName + ".jpg", ImageFormat.Jpeg);
            }
        }

        private static void DrawLine(Graphics g, Node from, Node to, Pen pen)
        {
            g.DrawLine(pen, from.Position.X * Scaling, from.Position.Y * Scaling, to.Position.X * Scaling, to.Position.Y * Scaling);
        }

        private Pen CreatePen(Color color, int width)
        {
            var pen = new Pen(color, width);
            pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            pen.StartCap = System.Drawing.Drawing2D.LineCap.Round;
            return pen;
        }

        private void Scale(int maxWidthPxl, int maxHeightPxl)
        {
            Height = Math.Abs(BB.MinY) + Math.Abs(BB.MaxY);
            Width = Math.Abs(BB.MinX) + Math.Abs(BB.MaxX);

            //No scaling
            if (maxWidthPxl == 0 && maxHeightPxl == 0) {
                PixelWidth = (int)Math.Floor(Width);
                PixelHeight = (int)Math.Floor(Height);
                return;
            }

            var scalingW = maxWidthPxl == 0 ? float.MaxValue : maxWidthPxl / Width;
            var scalingH = maxHeightPxl == 0 ? float.MaxValue : maxHeightPxl / Height;

            Scaling = Math.Min(scalingW, scalingH);

            PixelWidth = (int)Math.Floor(Width * Scaling);
            PixelHeight = (int)Math.Floor(Height * Scaling);

            Height *= Scaling;
            Width *= Scaling;
            BB.MaxX *= Scaling;
            BB.MaxY *= Scaling;
            BB.MinX *= Scaling;
            BB.MinY *= Scaling;
        }
    }
}
