# README #

Project for the course [Introduction to Artificial Intelligence](http://kurser.dtu.dk/course/02180) at the Technical University of Denmark.

Multiple pathfinding algorithms implemented to find paths in directed an undirected maps along with a simple visualization (exported images) of the search process and the shortest path that could be found.